<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class localizeEntry extends Model
{
    use HasFactory;
    protected $fillable = [
        'entryId',
        'cultureId',
        'status',
        'name',
        'misc001',
        'misc002',
        'misc003',
        'misc004',
        'misc005',
        'misc006',
        'misc007',
    ];

    public function Entries()
    {
        return $this->belongsTo(Entries::class);
    }
}
