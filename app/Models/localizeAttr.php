<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class localizeAttr extends Model
{ 
    use HasFactory;

    public function Attributes()
    {
        return $this->belongsTo(Attributes::class);
    }
}
