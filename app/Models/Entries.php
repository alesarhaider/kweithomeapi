<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Entries extends Model
{
    use HasFactory;
    protected $fillable = [
        'entryId',
        'attrId',
        'status',
        'addedBy',
    ];

    public function Attributes(): HasOne
    {
        return $this->hasOne(Attributes::class);
    }
    public function LocalizeEntry(): HasMany
    {
        return $this->hasMany(localizeEntry::class );
    }


}
