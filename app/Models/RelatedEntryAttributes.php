<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RelatedEntryAttributes extends Model
{
    use HasFactory;
    protected $fillable = [
        'entryId',
        'attrId',
        'priority',
        'status',
        'value',
        'addedBy',
        'updatedBy',
    ];
}
