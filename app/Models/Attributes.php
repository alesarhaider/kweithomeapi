<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Attributes extends Model
{
    use HasFactory;
    // each attributes may have multiple name values depending on cultureId
    public function LocalizeAttr(): HasMany
    {
        return $this->hasMany(localizeAttr::class , );
    }

    public function Entries() 
    {
        return $this->belongsTo(Entries::class);
    }
}
