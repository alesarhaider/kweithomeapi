<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RelatedEntries extends Model
{
    use HasFactory;
    protected $fillable = [
        'masterId',
        'slaveId',
        'priority',
        'status',
        'addedBy',
        'updatedBy',
    ];
}
