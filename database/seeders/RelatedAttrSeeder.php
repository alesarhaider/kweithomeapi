<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RelatedAttrSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('related_attributes')->insert([
            [
                'masterId' => '10101',
                'slaveId' => '501',
                'priority' => '0',
                'status' => '1',
                'addedBy'=>'22cb52ca-d47e-4658-b411-bc7cdefb3511',
                'updatedBy'=>'22cb52ca-d47e-4658-b411-bc7cdefb3511'
            ],
            [
                'masterId' => '10101',
                'slaveId' => '502',
                'priority' => '1',
                'status' => '1',
                'addedBy'=>'22cb52ca-d47e-4658-b411-bc7cdefb3511',
                'updatedBy'=>'22cb52ca-d47e-4658-b411-bc7cdefb3511'
            ],
            [
                'masterId' => '10101',
                'slaveId' => '503',
                'priority' => '1',
                'status' => '1',
                'addedBy'=>'22cb52ca-d47e-4658-b411-bc7cdefb3511',
                'updatedBy'=>'22cb52ca-d47e-4658-b411-bc7cdefb3511'
            ],
        ]);
    }
}
