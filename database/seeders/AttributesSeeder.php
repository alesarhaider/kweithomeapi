<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttributesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('attributes')->insert([
            [
                'attrId' => '0',
                'parentId' => '0',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '1',
                'parentId' => '0',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '2',
                'parentId' => '0',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '3',
                'parentId' => '0',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '4',
                'parentId' => '0',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '5',
                'parentId' => '0',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '101',
                'parentId' => '1',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '102',
                'parentId' => '1',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '103',
                'parentId' => '1',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '201',
                'parentId' => '2',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '202',
                'parentId' => '2',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '301',
                'parentId' => '3',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '302',
                'parentId' => '3',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '303',
                'parentId' => '3',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '401',
                'parentId' => '4',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '402',
                'parentId' => '4',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10101',
                'parentId' => '101',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10102',
                'parentId' => '101',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10103',
                'parentId' => '101',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10104',
                'parentId' => '101',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10201',
                'parentId' => '102',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10202',
                'parentId' => '102',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10203',
                'parentId' => '102',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10204',
                'parentId' => '102',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '104',
                'parentId' => '1',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10401',
                'parentId' => '104',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '105',
                'parentId' => '1',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10501',
                'parentId' => '105',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '10502',
                'parentId' => '105',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '403',
                'parentId' => '4',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '40301',
                'parentId' => '403',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '40302',
                'parentId' => '403',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '40303',
                'parentId' => '403',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '501',
                'parentId' => '5',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '502',
                'parentId' => '5',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '503',
                'parentId' => '5',
                'status' => '1',
                'priority' => '1',
            ], [
                'attrId' => '504',
                'parentId' => '5',
                'status' => '1',
                'priority' => '1',
            ]
        ]);
    }
}
