<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('related_entries', function (Blueprint $table) {
            $table->char('masterId',36);
            $table->char('slaveId',36);
            $table->integer('priority');
            $table->tinyInteger('status');
            $table->char('addedBy',36)->collation('ascii_general_ci');;
            $table->char('updatedBy',36)->collation('ascii_general_ci');;
            $table->timestamps();
        });
        
        Schema::table('related_entries', function ($table) {
            $table->foreign('masterId')->references('entryId')->on('entries')->onDelete('cascade');
            $table->foreign('slaveId')->references('entryId')->on('entries')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('related_entries');
    }
};
