<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('related_entry_attributes', function (Blueprint $table) {
            $table->char('entryId',36);
            $table->bigInteger('attrId');
            $table->integer('priority');
            $table->tinyInteger('status');
            $table->longText('value');
            $table->char('addedBy',36)->collation('ascii_general_ci');
            $table->char('updatedBy',36)->collation('ascii_general_ci');
            $table->timestamps();
        });
        Schema::table('related_entry_attributes', function ($table) {
            $table->foreign('entryId')->references('entryId')->on('entries')->onDelete('cascade');
            $table->foreign('attrId')->references('attrId')->on('attributes')->onDelete('cascade');
          
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('related_entry_attributes');
    }
};
