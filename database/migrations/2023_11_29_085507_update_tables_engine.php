<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement('ALTER TABLE related_attributes ENGINE= InnoDB');
        DB::statement('ALTER TABLE related_entries ENGINE= InnoDB');
        DB::statement('ALTER TABLE block_list ENGINE= InnoDB');
        DB::statement('ALTER TABLE exchanges ENGINE= InnoDB');
        DB::statement('ALTER TABLE localize_attrs ENGINE= InnoDB');
        DB::statement('ALTER TABLE localize_entries ENGINE= InnoDB');
        DB::statement('ALTER TABLE properties ENGINE= InnoDB');
        DB::statement('ALTER TABLE related_entry_attributes ENGINE= InnoDB');
        DB::statement('ALTER TABLE reports ENGINE= InnoDB');
        DB::statement('ALTER TABLE services ENGINE= InnoDB');
        DB::statement('ALTER TABLE users ENGINE= InnoDB');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
