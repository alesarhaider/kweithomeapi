<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('related_attributes', function (Blueprint $table) {
            $table->bigInteger('masterId');
            $table->bigInteger('slaveId');
            $table->integer('priority');
            $table->tinyInteger('status');
            $table->char('addedBy',36)->collation('ascii_general_ci');;
            $table->char('updatedBy',36)->collation('ascii_general_ci');;
            $table->timestamps();
            $table->primary(['masterId','slaveId']);
        });
        
        Schema::table('related_attributes', function (Blueprint $table) {
            $table->foreign('masterId')->references('attrId')->on('attributes')->onDelete('cascade');
            $table->foreign('slaveId')->references('attrId')->on('attributes')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('related_attributes_controllers');
    }
};
