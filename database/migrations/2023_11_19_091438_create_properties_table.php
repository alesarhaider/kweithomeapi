<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->char('entryId',36);
            $table->integer('area',false,true);
            $table->integer('bedrooms',false,true)->nullable();
            $table->integer('bathrooms',false,true)->nullable();
            $table->integer('driverroom',false,true)->nullable();
            $table->integer('maidroom',false,true)->nullable();
            $table->integer('street',false,true)->nullable();
            $table->timestamps();
     
        });
        
        Schema::table('properties', function ($table) {
            $table->foreign('entryId')->references('entryId')->on('entries')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('properties');
    }
};
