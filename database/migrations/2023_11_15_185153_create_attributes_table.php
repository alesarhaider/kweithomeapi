<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->bigInteger('attrId');
            $table->bigInteger('parentId');
            $table->tinyInteger('status');
            $table->tinyInteger('priority');
            $table->timestamps();
            $table->primary('attrId');
        });
        Schema::table('attributes', function ($table) {
            $table->index('parentId');
            $table->foreign('parentId')->references('attrId')->on('attributes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('attributes');
    }
};
