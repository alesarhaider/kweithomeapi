<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->char('userId', 36);
            $table->char('entryId', 36);
            $table->string('reason');
            $table->text('objection')->nullable();
            $table->timestamps();
        });
        
        Schema::table('reports', function ($table) {
            $table->foreign('userId')->references('unique_id')->on('users')->onDelete('no action');
            $table->foreign('entryId')->references('entryId')->on('entries')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('reports');
    }
};
