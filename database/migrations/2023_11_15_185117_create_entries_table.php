<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->char('entryId',36);
            $table->bigInteger('attrId');
            $table->tinyInteger('status');
            $table->char('addedBy',36)->collation('ascii_general_ci');;
            $table->char('updatedBy',36)->collation('ascii_general_ci');;
            $table->timestamps();
            $table->primary('entryId');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entries');
    }
};
