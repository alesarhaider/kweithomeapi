<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('block_list', function (Blueprint $table) {
            $table->id();
            $table->char('userId',36);
            $table->char('blockedId',36);
            $table->timestamps();
        });
        
        Schema::table('block_list', function ($table) {
            $table->foreign('userId')->references('unique_id')->on('users')->onDelete('no action');
            $table->foreign('blockedId')->references('unique_id')->on('users')->onDelete('no action');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('block_lists');
    }
};
