<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('localize_entries', function (Blueprint $table) {
            $table->char('entryId',36);
            $table->string('cultureId',2);
            $table->tinyInteger('status');
            $table->string('name',256);
            $table->string('misc001',512);
            $table->string('misc002',512);
            $table->string('misc003',512);
            $table->string('misc004',512);
            $table->string('misc005',512);
            $table->string('misc006',512);
            $table->string('misc007',512);           
            $table->timestamps();
        });
        
        Schema::table('localize_entries', function ($table) {
            $table->foreign('entryId')->references('entryId')->on('entries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('localize_entries');
    }
};
