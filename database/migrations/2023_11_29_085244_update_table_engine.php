<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        DB::statement('ALTER TABLE attributes ENGINE= InnoDB');
        DB::statement('ALTER TABLE entries ENGINE= InnoDB');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
